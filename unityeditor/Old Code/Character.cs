﻿public class OCharacter {

    private string title;
    private string slug;

    private int health;
    private int defense;
    private int attack;

    private int maxHealth;

    /*
    public Character()
    {

    }

    public Character(string title, string slug, int health, int attack, int defense)
    {
        this.title = title;
        this.slug = slug;

        this.health = health;
        this.maxHealth = health;
        this.attack = attack;
        this.defense = defense;
    }

    */

    //Getters
    public string GetCharacterName()
    {
        return this.title;
    }

    public string GetSlug()
    {
        return this.slug;
    }

    public int GetHealth()
    {
        return this.health;
    }

    public int GetMaxHealth()
    {
        return this.maxHealth;
    }

    public int GetDefense()
    {
        return this.defense;
    }

    public int GetAttack()
    {
        return this.attack;
    }

    //Setters

    public void SetHealth(int newHealth)
    {
        this.health = newHealth;
    }
}
