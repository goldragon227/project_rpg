﻿/*

using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System.IO;

public class ItemDatabase : MonoBehaviour {
    //Old
    private List<Item> database = new List<Item>();
    private JsonData itemData;
    //New



    void Start()
    {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        ConstructItemDatabase();

        Debug.Log(FetchItemByID(1).Description);
    }

    public Item FetchItemByID(int id)
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (database[i].ID == id)
            {
                return database[i];
            }
        }
        Debug.Log("Item of ID: " + id + " Not Found.");
        return null;
    }

    private void ConstructItemDatabase()
    {
        int itemID;
        string itemTitle;
        int itemValue;
        int itemAttack;
        int itemDefense;
        int itemVitality;
        string itemDescription;
        bool itemIsStackable;
        int itemRarity;
        string itemSlug;

        for (int i = 0; i < itemData.Count; i++)
        {
            itemID = (int)itemData[i]["id"];
            itemTitle = itemData[i]["title"].ToString();
            itemValue = (int)itemData[i]["value"];
            itemAttack = (int)itemData[i]["stats"]["attack"];
            itemDefense = (int)itemData[i]["stats"]["defense"];
            itemVitality = (int)itemData[i]["stats"]["vitality"];
            itemDescription = itemData[i]["description"].ToString();
            itemIsStackable = bool.Parse(itemData[i]["stackable"].ToString());
            itemRarity = (int)itemData[i]["rarity"];
            itemSlug = itemData[i]["slug"].ToString();


            database.Add(new Item(itemID, itemTitle, itemValue, itemAttack, itemDefense, itemVitality, itemDescription, itemIsStackable, itemRarity, itemSlug));
        }
    }
}

public class Item
{
    public int ID { get; set; }
    public string Title { get; set; }
    public int Value { get; set; }
    public int Attack { get; set; }
    public int Defense { get; set; }
    public int Vitality { get; set; }
    public string Description { get; set;}
    public bool IsStackable { get; set; }
    public int Rarity { get; set; }
    public string Slug { get; set; }

    public Item()
    {
        this.ID = -1;
    }

    public Item(int id, string title, int value, int attack, int defense, int vitality, string description, bool isStackable, int rarity, string slug)
    {
        this.ID = id;
        this.Title = title;
        this.Value = value;
        this.Attack = attack;
        this.Defense = defense;
        this.Vitality = vitality;
        this.Description = description;
        this.IsStackable = isStackable;
        this.Rarity = rarity;
        this.Slug = slug;
    }
}

*/