﻿using UnityEngine;
using System.Collections;
using System;

public class BattleControllerOLD : MonoBehaviour {

    public PlayerController player;

    private UIController _uiScript;
    private JSONDataHandler _database;
    private InputHandler _inputHandler;

    //GameObject collided with to start battle
    private GameObject _enemyObject;

    //Player Characters

    private NPC[] _characters = new NPC[4];
    private NPC[] _enemies = new NPC[4];

    private int _currentCharacter = 0;
    private int _currentEnemy = 4;
    private BattleAttack[] _Attacks = new BattleAttack[8];

    private bool _isPlayerPartyDead;
    private bool _isEnemyPartyDead;

    //Enemy Characters
    //Enemy enemy1;

    //Attacking
    //Enemy char1Attack;
    //Character enemy1Attack; 

    public enum BattleStates {
        Idle,
        Initialize,
        Player,
        PlayerAttack,
        PlayerSkill,
        PlayerSkillUse,
        PlayerItem,
        PlayerItemUse,
        PlayerTarget,
        Enemy,
        Resolve,
        PlayerWin,
        PlayerLose
    };

    private BattleStates _battleState = BattleStates.Idle;

    // Use this for initialization
    void Start () {
        _uiScript = gameObject.GetComponent<UIController>();
        _database = gameObject.GetComponent<JSONDataHandler>();
        _inputHandler = gameObject.GetComponent<InputHandler>();
	}
	
	// Update is called once per frame
	void Update () {
        Battle();
	}

    public void BattleStart(GameObject enemy)
    {
        _enemyObject = enemy;
        BattleState = BattleStates.Initialize;
    }

    void Battle()
    {
        switch (BattleState)
        {
            /* Idle is when there is no battle
             * Inititialize grabs the characters from the player and the information about the mobs
             */
            case BattleStates.Idle:
                break;
            case BattleStates.Initialize:
                InitializeBattle();
                LoadCombatants();
                InitializeBattleUI();
                _battleState = BattleStates.Player;
                break;
            case BattleStates.Player:
                //_inputHandler.BattlePlayer();
                break;
            case BattleStates.PlayerAttack:
                //_inputHandler.BattlePlayerAttack();
                break;
            case BattleStates.PlayerItem:
                break;
            case BattleStates.PlayerItemUse:
                break;
            case BattleStates.PlayerSkill:
                break;
            case BattleStates.PlayerSkillUse:
                break;
            case BattleStates.PlayerTarget:
                break;
            case BattleStates.Enemy:
                AIEnemy();
                _battleState = BattleStates.Resolve;
                break;
            case BattleStates.Resolve:
                //will run once
                ResolveBattle();
                ResetTurn();
                CheckIfEndBattle();
                break;
            case BattleStates.PlayerWin:
                /* Let the player exit the battle
                 * Close the battle UI
                 * Change the world state to Overworld
                 */
                _uiScript.SetBattlePanelVisibility(false);
                GameManager.GameState = GameManager.GameStates.OVERWORLD;
                BattleState = BattleStates.Idle;


                break;
            case BattleStates.PlayerLose:
                _uiScript.SetBattlePanelVisibility(false);
                GameManager.GameState = GameManager.GameStates.OVERWORLD;
                BattleState = BattleStates.Idle;
                break;
            default:
                break;
        }
    }

    private void CheckIfEndBattle()
    {
        if (_isPlayerPartyDead)
        {
            BattleState = BattleStates.PlayerLose;
        }
        else if (_isEnemyPartyDead)
        {
            BattleState = BattleStates.PlayerWin;
        }
        else
        {
            BattleState = BattleStates.Player;
        }
    }

    private void ResetTurn()
    {
        for (int i = 0; i < _Attacks.Length; i++)
        {
            _Attacks[i] = null;
        }

        _currentCharacter = 0;
        _currentEnemy = 4;

        _uiScript.SetBattleControlsVisisbility(true);
        //_inputHandler.MoveMarkerToNextLivingEnemy();
    }

    private void AIEnemy()
    {
        for (int i = 0; i < 4; i++)
        {
            if (!_enemies[i].IsDead)
            {
                _Attacks[_currentEnemy] = new BattleAttack(i, i, BattleAttack.AttackTypes.Melee, _enemies[i].Faction);
                _currentEnemy++;
            }
        }
    }

    private void ResolveBattle()
    {
        int damageCalc;
        int healthCalc;

        NPC attacker;
        NPC defender;

        for ( int i = 0; i < 8; i++)
        {
            if (!(_Attacks[i] == null))
            {
                Faction attackerFaction = _Attacks[i].GetAttackerFaction();

                switch (attackerFaction)
                {
                    case Faction.Player:
                        attacker = _characters[_Attacks[i].GetAttackerIndex()];
                        defender = _enemies[_Attacks[i].GetDefenderIndex()];
                        break;
                    case Faction.Enemy:
                        attacker = _enemies[_Attacks[i].GetAttackerIndex()];
                        defender = _characters[_Attacks[i].GetDefenderIndex()];
                        break;
                    default:
                        Debug.LogError("WARNING THIS NPC DOES NOT HAVE A FACTION");
                        attacker = new NPC(_database.GetNPCBySlug("default_npc"));
                        defender = new NPC(_database.GetNPCBySlug("default_npc"));
                        break;
                }

                if (!attacker.IsDead)
                {
                    int defenderIndex = _Attacks[i].GetDefenderIndex();

                    if (defender.IsDead)
                    {
                        switch (attackerFaction)
                        {
                            case Faction.Player:
                                defenderIndex = NextLivingEnemy();
                                defender = _enemies[NextLivingEnemy()];
                                break;
                            case Faction.Enemy:
                                defenderIndex = NextLivingCharacter();
                                defender = _characters[NextLivingCharacter()];
                                break;
                            default:
                                Debug.LogError("WARNING THIS NPC DOES NOT HAVE A FACTION");
                                attacker = new NPC(_database.GetNPCBySlug("default_npc"));
                                defender = new NPC(_database.GetNPCBySlug("default_npc"));
                                break;
                        }
                    }

                    //NPC attacker = _Attacks[i].GetAttackerIndex();
                    //NPC defender = _Attacks[i].GetDefenderIndex();

                    //Debug.LogError(attacker.GetName() + " I value: " + i + " Attack Power: " + attacker.GetAttack());

                    //Debug.Log("Attacker attack: " + attacker.GetAttack() + " Defender defense: " + defender.GetDefense() + "Estimated Damage: " + (attacker.GetAttack() - defender.GetDefense()));

                    damageCalc = 0;
                    damageCalc = attacker.Attack - defender.Defense;

                    if (damageCalc < 0)
                    {
                        damageCalc = 0;
                    }

                    //Debug.Log("DamageCalculated: " + damageCalc);

                    healthCalc = 0;
                    healthCalc = defender.Health - damageCalc;

                    defender.Health = healthCalc;

                    //Debug.Log("HealthCalculated: " + healthCalc);

                    //Debug.Log(attacker.GetName() + " damage to " + defender.GetName() + ": " + damageCalc);
                    //Debug.Log(defender.GetName() + " new health: " + healthCalc + " Should be: " + defender.GetHealth() + " / " + defender.GetMaxHealth());


                    if (defender.Health <= 0)
                    {
                        defender.IsDead = true;
                        _uiScript.SetNPCDead(defender, defenderIndex);
                        CheckIfAllDead(defender);

                        if (_isEnemyPartyDead || _isPlayerPartyDead)
                        {
                            return;
                        }
                    }

                    _uiScript.UpdateNPCHealth(defender, defenderIndex);
                }
            }

        }
    }

    private void CheckIfAllDead(NPC npc)
    {
        switch (npc.Faction)
        {
            case Faction.Player:
                for (int i = 0; i < _characters.Length; i++)
                {
                    if (!_characters[i].IsDead)
                    {
                        return;
                    }
                }

                Debug.Log("All players are dead.");
                _isPlayerPartyDead = true;
                
                break;
            case Faction.Enemy:
                for (int i = 0; i < _enemies.Length; i++)
                {
                    if (!_enemies[i].IsDead)
                    {
                        
                        return;
                    }
                }
                Debug.Log("All enemies are dead.");
                _isEnemyPartyDead = true;

                break;
        }
    }

    public void CharacterAttack(int enemy, BattleAttack.AttackTypes attackType)
    {
        _Attacks[_currentCharacter] = new BattleAttack(_currentCharacter, enemy, attackType, Faction.Player);

        _currentCharacter++;
        BattleState = BattleStates.Player;

        if (_currentCharacter == 4)
        {
            _uiScript.SetBattleControlsVisisbility(false);
            BattleState = BattleStates.Enemy;
        } else
        {
            _uiScript.SetBattleControlsVisisbility(true);
        }
    }

    public BattleStates BattleState
    {
        get
        {
            return _battleState;
        }

        set
        {
            _battleState = value;
        }
    }

    public NPC[] GetCharacters()
    {
        return _characters;
    }

    public NPC[] GetEnemies()
    {
        return _enemies;
    }

    public int NextLivingCharacter()
    {
        for (int i = 0; i < _characters.Length; i++)
        {
            if (!_characters[i].IsDead)
            {
                Debug.Log("Next alive character is: " + i);
                return i;
            }
        }
        return 0;
    }

    public int NextLivingEnemy()
    {
        for (int i = 0; i < _enemies.Length; i++)
        {
            if (!_enemies[i].IsDead)
            {
                Debug.Log("Next alive enemy is: " + i);
                return i;
            }
        }
        return 0;
    }

    public int NextLivingEnemy(int start, bool toRight)
    {
        if (toRight)
        {
            for (int i = start + 1; i < _enemies.Length; i++)
            {
                if (!_enemies[i].IsDead)
                {
                    Debug.Log("Next alive enemy is: " + i);
                    return i;
                }
            }
        } else
        {
            for (int i = start - 1; i > 0; i--)
            {
                if (!_enemies[i].IsDead)
                {
                    Debug.Log("Next alive enemy is: " + i);
                    return i;
                }
            }
        }
        return start;
    }

    private void InitializeBattle()
    {
        GameManager.GameState = GameManager.GameStates.BATTLE;
        _isEnemyPartyDead = false;
        _isPlayerPartyDead = false;

        /* Pause world
         * Load the battle UI/Level
         * Load Characters
         * Load Enemies
         * Load Drops and levels for enemies
         */
    }

    public void LoadCombatants()
    {
        print("People");

        //Player Characters
        //_characters = player.GetParty();
        print("Character 1: " + _characters[0].Name + "| Character 2: " + _characters[1].Name + "| Character 3: " + _characters[2].Name + "| Character 4: " + _characters[3].Name);

        //print("DEBUG character1 name: " + character1.GetCharacterName());

        //Enemy Characters

        for (int i = 0; i < _enemies.Length; i++)
        {
            _enemies[i] = new NPC(_database.GetNPCBySlug(_enemyObject.name.ToLower()));
            _enemies[i].Name = ("Slime " + i);
            print("Loaded " + _enemies[i].Name + " " + i);
        }

        //print("DEBUG enemy1 name: " + enemy1.GetTitle());
    }

    private void InitializeBattleUI()
    {
        _uiScript.SetBattlePanelVisibility(true);
        _uiScript.InitializeBattleSprites();

        for (int i = 0; i < _characters.Length; i++)
        {
            _uiScript.UpdateCharacterPanelName(_characters[i], i);
            _uiScript.UpdateNPCHealth(_characters[i], i);
        }

        for (int i = 0; i < _enemies.Length; i++)
        {
            _uiScript.UpdateEnemyPanelName(_enemies[i], i);
            _uiScript.UpdateNPCHealth(_enemies[i], i);
        }
    }

    //Old Code

    public void PlayerChooseAttack()
    {
        _battle.AllySelected = _battle.AllyGameObjects[_currentAttackingAlly];

        BattleAction action = new BattleAction(_battle.AllySelected, _battle.EnemySelected, ActionType.BasicAttack);

        NPC attackerNPC;
        NPC defenderNPC;

        switch (action.Attacker.tag)
        {
            case "Player":
                attackerNPC = action.Attacker.GetComponent<PlayerController>().NPC;
                break;
            case "Ally":
                attackerNPC = action.Attacker.GetComponent<Follower>().NPC;
                break;
            default:
                attackerNPC = database.GetNPCBySlug("default");
                break;
        }

        defenderNPC = action.Defender.GetComponent<Enemy>().NPC;

        int attackerDamage = attackerNPC.Attack - defenderNPC.Defense;

        if (attackerDamage < 0)
        {
            attackerDamage = 0;
        }

        int defenderHealth = defenderNPC.Health - attackerDamage;

        if (defenderHealth < 0)
        {
            defenderHealth = 0;
        }

        Debug.Log("Defender " + defenderNPC.Name + " took " + attackerDamage + " damage from " + attackerNPC.Name);
        defenderNPC.Health = defenderHealth;

        _currentAttackingAlly++; //Switch to the next ally attacker


        //Check if all players have attacked

        if (_currentAttackingAlly >= _battle.Allies.Count)
        {
            BattleState = BattleStates.EnemyTurn;
        }

        BattleState = BattleStates.PlayerTurn;
    }
}