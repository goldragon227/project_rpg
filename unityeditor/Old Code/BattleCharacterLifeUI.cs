﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleCharacterLifeUI : MonoBehaviour {

    private RectTransform _currentHealthBarTransform;
    private float _healthBarXMax;

    private RectTransform _currentManaBarTransform;
    private float _manaBarXMax;

    private Text _currentHealthText;
    private bool _isHealthTextVisible = true;

    private Text _currentManaText;
    private bool _isManaTextVisible = true;

	// Use this for initialization
	void Start () {
        //Debug.Log(gameObject);

        _currentHealthBarTransform = gameObject.transform.Find("HealthBack/Health").gameObject.GetComponent<RectTransform>();
        _currentManaBarTransform = gameObject.transform.Find("ManaBack/Mana").gameObject.GetComponent<RectTransform>();

        //Debug.Log(_currentHealthBarTransform);
        //Debug.Log(_currentManaBarTransform);

        _currentHealthText = gameObject.transform.Find("Health Text").gameObject.GetComponent<Text>();
        _currentManaText = gameObject.transform.Find("Mana Text").gameObject.GetComponent<Text>();

        _healthBarXMax = (_currentHealthBarTransform.rect.width * -1.0f);
        _manaBarXMax = (_currentManaBarTransform.rect.width * -1.0f);
    }

    public void SetHealth(int maxHealth, int health)
    {
        SetHealthText(maxHealth, health);
        SetHealthBar(health / maxHealth);
    }

    public void SetMana(int maxMana, int mana)
    {
        SetManaText(maxMana, mana);
        SetManaBar(mana / maxMana);
    }


    private void SetHealthText(int maxHealth, int health)
    {
        _currentHealthText.text = (health + " / " + maxHealth);
        
    }

    private void SetManaText(int maxMana, int mana)
    {
        _currentManaText.text = (mana + " / " + maxMana);
    }

    private void SetHealthBar(float percent)
    {
        Vector3 playerHealthImagePos = Vector3.right * _healthBarXMax * (1.0f - percent);
        //Debug.Log("PlayerHealthImagePos: " + PlayerHealthImagePos);

        Vector3 workingVector = (new Vector3() - _currentHealthBarTransform.position);
        //Debug.Log("WorkingVector: " + WorkingVector);

        _currentHealthBarTransform.position += workingVector + playerHealthImagePos;
    }

    private void SetManaBar(float percent)
    {
        Vector3 playerManaImagePos = Vector3.right * _manaBarXMax * (1.0f - percent);
        //Debug.Log("PlayerHealthImagePos: " + PlayerHealthImagePos);

        Vector3 workingVector = (new Vector3() - _currentManaBarTransform.position);
        //Debug.Log("WorkingVector: " + WorkingVector);

        _currentManaBarTransform.position += workingVector + playerManaImagePos;
    }



}
