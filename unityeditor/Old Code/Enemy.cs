﻿public class Enemy
{
    //Initial Stats
    private string slug;
    private string title;

    private int health;
    private int attack;
    private int defense;

    private int maxHealth;

    /* TODO
     * Exp Gain
     * Item drops
     *   */

    //Calculated Stats
    int Level;

    public Enemy(string nslug, string ntitle, int nhealth, int nattack, int ndefense)
    {
        this.slug = nslug;
        this.title = ntitle;

        this.health = nhealth;
        this.maxHealth = nhealth;
        this.attack = nattack;
        this.defense = ndefense;
    }


    public string GetSlug()
    {
        return this.slug;
    }

    public string GetTitle()
    {
        return this.title;
    }

    public int GetHealth()
    {
        return this.health;
    }

    public int GetAttack()
    {
        return this.attack;
    }

    public int GetDefense()
    {
        return this.defense;
    }

    public int GetMaxHealth()
    {
        return this.maxHealth;
    }

    //Setters

    public void SetHealth(int newHealth)
    {
        this.health = newHealth;
    }
}
