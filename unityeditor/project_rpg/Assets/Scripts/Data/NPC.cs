﻿using UnityEngine;

public enum Faction
{
    Default,
    Player,
    Enemy
}

public class NPC
{
    private GameObject _gameObject;

    private string _name;
    private string _slug;

    private Faction _faction;

    private int _level;
    private int _exp;
    private int _expToLevel;

    private int _health;
    private int _maxHealth;

    private int _mana;
    private int _maxMana;

    private int _attack;
    private int _defense;

    private int _magicAttack;
    private int _magicDefense;

    private int _speed;

    //Used to determine what NPC will go next in battle, Once this value reachs a point the NPC can attack
    private float _speedTotal;

    private bool isDead = false;

    public NPC()
    {

    }

    public NPC(string name, string slug, int health, int attack, int defense, int speed, Faction faction)
    {
        this.Name = name;
        this.Slug = slug;

        this.Health = health;
        this.MaxHealth = health;
        this.Attack = attack;
        this.Defense = defense;
        this.Speed = speed;
        this.Faction = faction;
    }

    public NPC(NPC npc)
    {
        this.Name      = npc.Name;
        this.Slug      = npc.Slug;

        this.Health    = npc.Health;
        this.MaxHealth = npc.Health;
        this.Attack    = npc.Attack;
        this.Defense   = npc.Defense;
        this.Speed     = npc.Speed;

        this.Faction   = npc.Faction;
    }


    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }

    public string Slug
    {
        get
        {
            return _slug;
        }

        set
        {
            _slug = value;
        }
    }

    public Faction Faction
    {
        get
        {
            return _faction;
        }

        set
        {
            _faction = value;
        }
    }

    public int Health
    {
        get
        {
            return _health;
        }

        set
        {
            _health = value;

            if (_health < 0)
            {
                _health = 0;
            }
        }
    }

    public int MaxHealth
    {
        get
        {
            return _maxHealth;
        }

        set
        {
            _maxHealth = value;
        }
    }

    public int Attack
    {
        get
        {
            return _attack;
        }

        set
        {
            _attack = value;
        }
    }

    public int Defense
    {
        get
        {
            return _defense;
        }

        set
        {
            _defense = value;
        }
    }

    public bool IsDead
    {
        get
        {
            return isDead;
        }

        set
        {
            isDead = value;
        }
    }

    public int Speed
    {
        get
        {
            return _speed;
        }

        set
        {
            _speed = value;
        }
    }

    public GameObject GameObject
    {
        get
        {
            return _gameObject;
        }

        set
        {
            _gameObject = value;
        }
    }

    public float SpeedTotal
    {
        get
        {
            return _speedTotal;
        }

        set
        {
            _speedTotal = value;
        }
    }

    /*
    //Getters
    public string GetName()
    {
        return Name;
    }

    public string GetSlug()
    {
        return this._slug;
    }

    public int GetLevel()
    {
        return this._level;
    }

    public int GetHealth()
    {
        return this._health;
    }

    public int GetMaxHealth()
    {
        return this._maxHealth;
    }

    public int GetAttack()
    {
        return this._attack;
    }

    public int GetMana()
    {
        return this._mana;
    }

    public int GetDefense()
    {
        return this._defense;
    }

    public Faction GetFaction()
    {
        return this._faction;
    }

    //Setters
    public void SetHealth(int health)
    {
        this._health = health;
    }

    public void SetName(string name)
    {
        this.Name = name;
    }
    */
}
