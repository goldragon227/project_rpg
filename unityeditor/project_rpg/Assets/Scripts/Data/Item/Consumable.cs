﻿using System.Collections.Generic;

public class Consumable : Item
{
    public Consumable(string slug, string name, string description, int value, bool isStackable, List<Effect> effects)
    {
        Slug = slug;
        Name = name;
        Description = description;
        Value = value;

        IsStackable = isStackable;
        _effects = effects;
    }

    public Consumable(Consumable origin)
    {
        Slug = origin.Slug;
        Name = origin.Name;
        Description = origin.Description;
        Value = origin.Value;

        IsStackable = origin.IsStackable;
        _effects = origin.Effects;
    }
}