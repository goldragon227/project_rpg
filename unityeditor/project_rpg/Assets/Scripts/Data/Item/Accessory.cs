﻿
//Sinlge Accessory
//Multiple Accessories

public class Accessory : Item
{
    private string _effectID;
    private int _effectPower;

    public Accessory(string slug, string name, string description, int value, string effectID, int effectPower)
    {
        this.Slug = slug;
        this.Name = name;
        this.Description = description;
        this.Value = value;

        this.effectID = effectID;
        this.effectPower = effectPower;
    }

    public string effectID
    {
        get
        {
            return _effectID;
        }

        set
        {
            _effectID = value;
        }
    }

    public int effectPower
    {
        get
        {
            return _effectPower;
        }

        set
        {
            _effectPower = value;
        }
    }
}