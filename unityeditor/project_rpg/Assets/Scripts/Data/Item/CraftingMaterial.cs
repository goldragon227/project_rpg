﻿using UnityEngine;

public class CraftingMaterial : Item
{
    public CraftingMaterial(string slug, string name, string description, int value)
    {
        this.Slug = slug;
        this.Name = name;
        this.Description = description;
        this.Value = value;
    }
}