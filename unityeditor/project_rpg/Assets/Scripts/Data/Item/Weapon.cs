﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item {

    private bool _isRanged; // 0 for Melee or 1 for ranged
    private int _attack;
    private int _magicAttack;

    public Weapon(string slug, string name, string description, bool isRanged, int value, int attack, int magicAttack)
    {
        this.Slug =        slug;
        this.Name =        name;
        this.Description = description;
        this.Value =       value;
        this.isRanged =    isRanged;
        this.attack =      attack;
        this.magicAttack = magicAttack;
    }

    public Weapon(Weapon other)
    {
        Slug =        other.Slug;
        Name =        other.Name;
        Description = other.Description;
        Value =       other.Value;
        isRanged =    other.isRanged;
        attack =      other.attack;
        magicAttack = other.magicAttack;
    }

    public bool isRanged
    {
        get
        {
            return _isRanged;
        }

        set
        {
            _isRanged = value;
        }
    }

    public int attack
    {
        get
        {
            return _attack;
        }

        set
        {
            _attack = value;
        }
    }

    public int magicAttack
    {
        get
        {
            return _magicAttack;
        }

        set
        {
            _magicAttack = value;
        }
    }
}
