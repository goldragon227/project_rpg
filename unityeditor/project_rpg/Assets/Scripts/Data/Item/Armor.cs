﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Item {

    private int _armorSlot;
    private int _defense;
    private int _magicDefense;

    public Armor(string slug, string name, string description, int armorSlot, int value, int defense, int magicDefense)
    {
        this.Slug =        slug;
        this.Name =        name;
        this.Description = description;
        this.Value =       value;

        this.armorSlot =    armorSlot;
        this.magicDefense = magicDefense;
        this.defense =      defense;
    }

    public Armor(Armor other)
    {
        Slug =        other.Slug;
        Name =        other.Name;
        Description = other.Description;
        Value =       other.Value;

        armorSlot =    other.armorSlot;
        magicDefense = other.magicDefense;
        defense =      other.defense;
    }

    public int armorSlot
    {
        get
        {
            return _armorSlot;
        }

        set
        {
            _armorSlot = value;
        }
    }

    public int defense
    {
        get
        {
            return _defense;
        }

        set
        {
            _defense = value;
        }
    }

    public int magicDefense
    {
        get
        {
            return _magicDefense;
        }

        set
        {
            _magicDefense = value;
        }
    }
}
