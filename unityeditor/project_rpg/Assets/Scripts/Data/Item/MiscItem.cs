﻿public class MiscItem : Item
{
    public MiscItem(string slug, string name, string description, int value)
    {
        this.Slug = slug;
        this.Name = name;
        this.Description = description;
        this.Value = value;
    }
}