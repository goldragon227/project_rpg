﻿using System.Collections.Generic;
using UnityEngine;

public class Item {

    private string _name;
    private string _slug;
    private string _description;
    private int _value;

    private float _weight;

    private bool _isStackable;
    private int _count = 1;


    protected List<Effect> _effects = new List<Effect>();

    //Image

    public void AddEffect(Effect effect)
    {
        _effects.Add(effect);
    }

    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }

    public string Slug
    {
        get
        {
            return _slug;
        }

        set
        {
            _slug = value;
        }
    }

    public string Description
    {
        get
        {
            return _description;
        }

        set
        {
            _description = value;
        }
    }

    public int Value
    {
        get
        {
            return _value;
        }

        set
        {
            _value = value;
        }
    }

    public List<Effect> Effects
    {
        get
        {
            return _effects;
        }
    }

    public bool IsStackable
    {
        get
        {
            return _isStackable;
        }

        set
        {
            _isStackable = value;
        }
    }

    public int Count
    {
        get
        {
            return _count;
        }

        set
        {
            _count = value;
        }
    }
}
