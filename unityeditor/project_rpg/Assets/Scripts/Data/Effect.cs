﻿using System.Collections.Generic;

public class Effect {

    private string _effectID;
    private int _effectPower;

    public Effect(string effectID, int effectPower)
    {
        _effectID = effectID;
        _effectPower = effectPower;
    }
}
