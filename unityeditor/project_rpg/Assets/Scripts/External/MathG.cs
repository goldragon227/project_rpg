﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathG{

    public static float Distance(Rigidbody2D origin, Rigidbody2D destination)
    {
        float x1 = origin.transform.position.x;
        float x2 = destination.transform.position.x;

        float y1 = origin.transform.position.y;
        float y2 = destination.transform.position.y;

        float x3 = (x2 - x1);
        float y3 = (y2 - y1);

        x3 = Mathf.Pow(x3, 2);
        y3 = Mathf.Pow(y3, 2);

        float distance = Mathf.Sqrt(x3 + y3);

        return distance;
    }

    public static Vector2 Vector2(Rigidbody2D origin, Rigidbody2D destination)
    {
        float x1 = origin.transform.position.x;
        float x2 = destination.transform.position.x;

        float y1 = origin.transform.position.y;
        float y2 = destination.transform.position.y;

        return new Vector2(x2 - x1, y2 - y1);
    }

    public static Vector2 UnitVector2(Rigidbody2D origin, Rigidbody2D destination)
    {
        Vector2 output = Vector2(origin, destination);

        float x = output.x;
        float y = output.y;

        float divisor = Mathf.Sqrt(Mathf.Pow(x,2) + Mathf.Pow(y, 2));

        output = output / divisor;

        return output;
    }
}
