﻿using UnityEngine;
using UnityEngine.UI;

public class FPSDisplay : MonoBehaviour {

    private FPSCounter _fpsCounter;
    public Text _fpsText;


	// Use this for initialization
	void Start () {
        _fpsCounter = gameObject.GetComponent<FPSCounter>();
	}
	
	// Update is called once per frame
	void Update () {
        _fpsText.text = Mathf.Clamp(_fpsCounter.FPS, 0, 999).ToString();
	}
}
