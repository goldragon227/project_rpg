﻿using UnityEngine;

public class FPSCounter : MonoBehaviour {

    private int _fps;

    public int FPS
    {
    get { return _fps; }
    set { _fps = value; }
    }

    // Update is called once per frame
    void Update () {
        _fps = (int) (1f / Time.unscaledDeltaTime);
	}
}
