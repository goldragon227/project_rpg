﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryData {

    private float _weight;
    private float _weightMax;

    List<Item> _items = new List<Item>();

    public void AddItem(Item item)
    {
        //Check the total weight to see if the Item can be added
        //If the total inventory weight does not exceed the max weight, add the item

        if(_items.Count > 0)
        {
            foreach (Item element in _items)
            {
                if(element.Name == item.Name)
                {
                    if (item.IsStackable)
                    {
                        //Debug.Log("Added " + item.Count + " to the stack of " + item.Name );
                        element.Count += item.Count;
                        //Debug.Log("Total number of " + element.Name + ":  " + element.Count);
                        return;
                    }
                    _items.Add(item);
                    return;
                }
            }
        }
        
        _items.Add(item); 
    }

    //Remove by slug or ID
    public void RemoveItem()
    {
        //Remove item and remove the items weight
    }

}
