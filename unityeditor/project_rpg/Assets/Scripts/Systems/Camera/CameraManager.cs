﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    Transform playerTransform;
    Camera currentCamera;

    public float zoomModifier = 10f;    
    public float maxZoomOut = 40f;
    public float maxZoomIn = 1f;

    // Use this for initialization
    void Start () {
        currentCamera = gameObject.GetComponent<Camera>();

        playerTransform = GameObject.Find("Player").transform;
        Debug.Log("Camera Loaded");
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("X: "+playerTransform.position.x + " Y: " + playerTransform.position.y);
       
        gameObject.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, gameObject.transform.position.z);
        Zoom();
    }


    void Zoom()
    {
        float axis = Input.GetAxis("Mouse ScrollWheel");

        if(axis > 0)
        {
            if(currentCamera.orthographicSize < maxZoomOut)
            {
                currentCamera.orthographicSize += axis * zoomModifier;
            }
            
        }
        else if(axis < 0)
        {
            if (currentCamera.orthographicSize > maxZoomIn)
            {
                currentCamera.orthographicSize += axis * zoomModifier;
            }
        }
    }
}
