﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUI : MonoBehaviour {

    public GameObject DebugRaycastPanel;
    public GameObject DebugBattleAllies;
    public GameObject DebugBattleEnemies;

    public Text battleTextAllies;
    public Text battleTextEnemies;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DisplayFoundEnemies(RaycastHit2D[] hits)
    {
        Text textElement = DebugRaycastPanel.transform.Find("Raycast Debug").gameObject.GetComponent<Text>();
        string output = "";

        foreach (RaycastHit2D element in hits)
        {
            output += element.transform.gameObject.ToString() + "\n";
        }

        textElement.text = output;
    }

    internal void DisplayAllyInfo(List<GameObject> allies)
    {
        string output = "";
        foreach (GameObject element in allies)
        {
            NPC npc;
            switch (element.gameObject.tag)
            {
                case "Player":
                    npc = element.gameObject.GetComponent<PlayerManager>().npc;
                    break;
                case "Ally":
                    npc = element.gameObject.GetComponent<Follower>().NPC;
                    break;
                default:
                    npc = new NPC();
                    break;
            }
            

            output += element.gameObject + "\n" + npc.Name + " HP: " + npc.Health + " / " + npc.MaxHealth + "\n\n";
        }

        battleTextAllies.text = output;
    }

    internal void DisplayEnemyInfo(List<GameObject> enemies)
    {
        string output = "";
        foreach (GameObject element in enemies)
        {
            NPC npc = element.gameObject.GetComponent<Enemy>().NPC;

            output += element.gameObject + "\n" + npc.Name + " HP: " + npc.Health + " / " + npc.MaxHealth + "\n\n";
        }

        battleTextEnemies.text = output;
    }
}
