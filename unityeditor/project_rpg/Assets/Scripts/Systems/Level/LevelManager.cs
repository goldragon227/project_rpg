﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public float autoLoadNextLevelAfter;

    void Start()
    {
        if (autoLoadNextLevelAfter == 0f)
        {
            Debug.Log("Autoload disabled.");
        }
        else
        {
            Invoke("LoadNextLevel", autoLoadNextLevelAfter);
        }
    }

    public void LoadLevelString(string name)
    {
        Debug.Log("New Level load: " + name);
        //Application.LoadLevel(name) Legacy;
        SceneManager.LoadScene(name);
    }

    public void LoadLevelBuildIndex(int index)
    {
        Debug.Log("New Level load: " + index);
        SceneManager.LoadScene(index);
    }

    public void QuitRequest()
    {
        Debug.Log("Quit requested");
        Application.Quit();
    }

    public void LoadNextLevel()
    {
        //Application.LoadLevel(Application.loadedLevel + 1) Legacy;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public string GetSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }
}
