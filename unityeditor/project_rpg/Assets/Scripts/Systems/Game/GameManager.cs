﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public enum GameStates
    {
        OVERWORLD,
        BATTLE,
        SHOP,
        PAUSED
    }

    private static bool _isPaused = false;
    private static GameStates _gameState = GameStates.OVERWORLD;
    private static GameManager _instance = null;

    private BattleManager battleController;
    private BattleUI battleUI;

    // Use this for initialization
    void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            print("Duplicate player self-destructing!");
        }
        else
        {
            _instance = this;
            GameObject.DontDestroyOnLoad(gameObject);

            //battleScript = gameObject.GetComponent<BattleController>();
            //uiScript = gameObject.GetComponent<UIController>();

        }
    }

    private void Update()
    {
        switch (GameState)
        {
            case GameStates.OVERWORLD:
                break;
            case GameStates.BATTLE:
                break;
            case GameStates.SHOP:
                break;
            case GameStates.PAUSED:
                break;
        }
    }

    public static void TogglePause()
    {
        _isPaused = !_isPaused;
    }

    public static GameStates GameState
    {
        get
        {
            return _gameState;
        }

        set
        {
            Debug.Log("Game state is now: " + GameState);
            _gameState = value;
        }
    }
}
