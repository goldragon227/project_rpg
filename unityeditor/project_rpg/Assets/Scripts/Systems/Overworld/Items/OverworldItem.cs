﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class OverworldItem : MonoBehaviour {

    private JSONManager database;
    private Item item;

    public string itemType;
    public string itemSlug;

    public static int Popup;

	// Use this for initialization
	void Start () {
        database = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<JSONManager>();

        switch (itemType)
        {
            case "consumable":
                item = database.GetConsumableBySlug(itemSlug);
                break;
            case "armor":
                item = database.GetArmorBySlug(itemSlug);
                break;
            case "weapon":
                item = database.GetWeaponBySlug(itemSlug);
                break;
            case "misc_item":
                item = database.GetMiscItemBySlug(itemSlug);
                break;
            default:
                Debug.Log(gameObject.name + "ERROR NO ITEM");
                break;
        }
        //Get item from database
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            collision.GetComponent<PlayerManager>().Data.Inventory.AddItem(item);
            //Get the player manager
            //Add the ability to have a stack of multiple potions
            //Later have a special image for multiple items
            //Add a item to the playerData.inventoryData.addItem()
            Debug.Log("Added " + item.Name + " to " + collision.name + "'s Inventory.");
            Destroy(gameObject);
        }        
    }

}
