﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour {

    private JSONManager _database;
    private NPC _NPC;

    private Rigidbody2D leaderRigidbody;
    private Rigidbody2D followerRigidbody;

    private GameObject player;

    [SerializeField]
    private float distanceFromPlayer;

    [SerializeField]
    private Vector2 _unitVectorToPlayer;

    public float maxDistance = 10;
    public float minDistance = 2f;
    public int speed;

    // Use this for initialization
    void Start () {
        _database = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<JSONManager>();

        FindRigidbodies();
        FindNPCObject();
    }
	
	// Update is called once per frame
	void Update ()
    {

        switch (GameManager.GameState)
        {
            case GameManager.GameStates.OVERWORLD:
                distanceFromPlayer = MathG.Distance(followerRigidbody, leaderRigidbody);
                _unitVectorToPlayer = MathG.UnitVector2(followerRigidbody, leaderRigidbody);


                if (distanceFromPlayer > maxDistance)
                {
                    followerRigidbody.position += _unitVectorToPlayer * Time.deltaTime * speed;
                }
                if (distanceFromPlayer < minDistance)
                {
                    followerRigidbody.position -= _unitVectorToPlayer * Time.deltaTime * speed;
                }
                break;
            case GameManager.GameStates.BATTLE:
                break;
        }

    }

    private void FindRigidbodies()
    {
        followerRigidbody = gameObject.GetComponent<Rigidbody2D>();

        player = GameObject.FindGameObjectWithTag("Player");
        //leaderRigidbody = leader.GetComponent<Rigidbody2D>();

        leaderRigidbody = player.GetComponent<Rigidbody2D>();
    }

    //Assuming the follower had its gameObject name set correctly
    //Finds the corrisponding NPC in the NPC database

    private void FindNPCObject()
    {
        NPC = _database.GetNPCBySlug(gameObject.name.ToLower());
    }

    public NPC NPC
    {
        get
        {
            return _NPC;
        }

        set
        {
            _NPC = value;
        }
    }
}
