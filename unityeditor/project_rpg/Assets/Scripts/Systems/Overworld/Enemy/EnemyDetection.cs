﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour {

    private Enemy _enemy;


    //Sends the player gameObject to the Enemy Parent if it is triggered
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //Debug.Log(collision.gameObject);
            Enemy.PlayerDetected(collision.gameObject);
        }
    }

    public Enemy Enemy
    {
        get { return _enemy; }
        set {_enemy = value; }
    }
}
