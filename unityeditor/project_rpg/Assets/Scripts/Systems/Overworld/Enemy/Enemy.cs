﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    private JSONManager _database;

    private NPC _NPC;
    private EnemyDetection _playerDetection;
    private CircleCollider2D _detectionCollider;

    private Rigidbody2D _rigidbody2D;
    private Rigidbody2D _playerRigidbody2D;

    private GameObject _player;


    [Header("Settings")]
    public int _speed;
    public float _maxFollowDistance = 10f;


    [Header("Follow Information")]
    [SerializeField]
    private bool _isFollowingPlayer = false;
    [SerializeField]
    private float _distanceFromPlayer;
    [SerializeField]
    private Vector2 _unitVectorToPlayer;


    [Header("Detection Radius")]
    public bool reloadRadius = false;
    [SerializeField]
    private float _detectionRadius;



    // Use this for initialization
    void Start () {
        _rigidbody2D = gameObject.GetComponent<Rigidbody2D>();

        GameObject detectionCircle = transform.Find("Detection Circle").gameObject;
        _playerDetection = detectionCircle.GetComponent<EnemyDetection>();
        _detectionCollider = detectionCircle.GetComponent<CircleCollider2D>();

        ForceRadiusReload();
        _playerDetection.Enemy = this;

        _database = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<JSONManager>();

        char[] delimiterChars = { ' ' };
        NPC = _database.GetNPCBySlug(name.Split(delimiterChars)[0].ToLower());
    }
	
	// Update is called once per frame
	void Update () {

        if (!NPC.IsDead)
        {
            switch (GameManager.GameState)
            {
                case GameManager.GameStates.OVERWORLD:
                    if (IsFollowingPlayer)
                    {
                        _distanceFromPlayer = MathG.Distance(_rigidbody2D, _playerRigidbody2D);
                        _unitVectorToPlayer = MathG.UnitVector2(_rigidbody2D, _playerRigidbody2D);

                        _rigidbody2D.position += _unitVectorToPlayer * Time.deltaTime * _speed;
                    }

                    if (_distanceFromPlayer > _maxFollowDistance)
                    {
                        IsFollowingPlayer = false;
                    }

                    if (reloadRadius)
                    {
                        ForceRadiusReload();
                        reloadRadius = false;
                    }
                    break;
                case GameManager.GameStates.BATTLE:
                    break;
            }
        }

    }

    public void PlayerDetected(GameObject player)
    {
        _player = player;
        IsFollowingPlayer = true;
        _playerRigidbody2D = _player.GetComponent<Rigidbody2D>();
        
    }


    public void ForceRadiusReload()
    {
        _detectionCollider.radius = _detectionRadius;
    }



    public float DetectionRadius
    {
        get
        {
            return _detectionRadius;
        }

        set
        {
            _detectionRadius = value;
            _detectionCollider.radius = value;
        }
    }

    public NPC NPC
    {
        get
        {
            return _NPC;
        }

        set
        {
            _NPC = value;
        }
    }

    public bool IsFollowingPlayer
    {
        get
        {
            return _isFollowingPlayer;
        }

        set
        {
            _isFollowingPlayer = value;
        }
    }
}
