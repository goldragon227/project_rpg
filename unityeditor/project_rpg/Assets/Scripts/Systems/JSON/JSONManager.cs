﻿using UnityEngine;
using LitJson;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System;

public class JSONManager : MonoBehaviour
{

    private List<Armor> ArmorDatabase = new List<Armor>();
    private JsonData armorData;

    private List<Weapon> WeaponDatabase = new List<Weapon>();
    private JsonData weaponData;

    private List<MiscItem> MiscItemDatabase = new List<MiscItem>();
    private JsonData miscItemData;

    private List<Accessory> AccessoryDatabase = new List<Accessory>();
    private JsonData accessoryData;

    private List<NPC> NPCDatabase = new List<NPC>();
    private JsonData NPCData;

    private List<Consumable> ConsumableDatabase = new List<Consumable>();
    private JsonData consumableData;

    //Effects
    private JsonData effectData;
    //Skills

    /*
    private List<Enemy> EnemyDatabase = new List<Enemy>();
    private JsonData enemyData;

    private List<Character> CharacterDatabase = new List<Character>();
    private JsonData characterData;
    */

    // Use this for initialization
    void Awake()
    {

        armorData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Armor.json"));
        weaponData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Weapons.json"));
        miscItemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/MiscItems.json"));
        accessoryData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Accessories.json"));
        NPCData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/NPC.json"));
        consumableData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Consumable.json"));

        //enemyData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Enemys.json"));
        //characterData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Characters.json"));
        Debug.Log("JSON Data Loaded");

        ConstructArmorDatabase();
        ConstructWeaponDatabase();
        ConstructMiscItemDatabase();
        ConstructAccessoryDatabase();
        ConstructNPCDatabase();
        ConstructConsumableDatabase();
        //ConstructEnemyDatabase();
        //ConstructCharacterDatabase();

        Debug.Log("Databases constructed");

        //Debug.Log(GetNPCBySlug("slime").GetSlug());
    }

    private void ConstructArmorDatabase()
    {
        string slug;
        string title;
        string description;
        int armorSlot;
        int value;
        int defense;
        int magicDefense;

        for (int i = 0; i < armorData.Count; i++)
        {
            slug = armorData[i]["slug"].ToString();
            title = armorData[i]["title"].ToString();
            description = armorData[i]["description"].ToString();
            armorSlot = (int)armorData[i]["armor_slot"];
            value = (int)armorData[i]["value"];
            defense = (int)armorData[i]["stats"]["defense"];
            magicDefense = (int)armorData[i]["stats"]["magic_defense"];

            ArmorDatabase.Add(new Armor(slug, title, description, armorSlot, value, defense, magicDefense));
        }
    }

    private void ConstructNPCDatabase()
    {
        string slug;
        string title;

        //TODO
        //string description;

        int health;
        int attack;
        int defense;

        int speed;

        string factionString;
        Faction faction;

        for (int i = 0; i < NPCData.Count; i++)
        {
            slug = NPCData[i]["slug"].ToString();
            title = NPCData[i]["title"].ToString();

            health = (int)NPCData[i]["stats"]["health"];
            attack = (int)NPCData[i]["stats"]["attack"];
            defense = (int)NPCData[i]["stats"]["defense"];
            speed = (int)NPCData[i]["stats"]["speed"];

            factionString = NPCData[i]["faction"].ToString();

            if (factionString == "player")
            {
                faction = Faction.Player;
            }
            else if (factionString == "enemy")
            {
                faction = Faction.Enemy;
            }
            else
            {
                faction = Faction.Default;
            }

            NPCDatabase.Add(new NPC(title, slug, health, attack, defense, speed, faction));
        }
    }

    private void ConstructWeaponDatabase()
    {
        string slug;
        string title;
        string description;
        int value;
        int isRanged;
        bool isRangedBool;
        int attack;
        int magicAttack;

        for (int i = 0; i < weaponData.Count; i++)
        {
            slug = weaponData[i]["slug"].ToString();
            title = weaponData[i]["title"].ToString();
            description = weaponData[i]["description"].ToString();
            value = (int)weaponData[i]["value"];

            isRanged = (int)weaponData[i]["is_ranged"];
            attack = (int)weaponData[i]["stats"]["attack"];
            magicAttack = (int)weaponData[i]["stats"]["magic_attack"];

            switch (isRanged)
            {
                case 0:
                    isRangedBool = false;
                    break;
                case 1:
                    isRangedBool = true;
                    break;
                default:
                    isRangedBool = false;
                    break;

            }


            WeaponDatabase.Add(new Weapon(slug, title, description, isRangedBool, value, attack, magicAttack));
        }
    }

    private void ConstructMiscItemDatabase()
    {
        string slug;
        string title;
        string description;
        int value;

        for (int i = 0; i < miscItemData.Count; i++)
        {
            slug = miscItemData[i]["slug"].ToString();
            title = miscItemData[i]["title"].ToString();
            description = miscItemData[i]["description"].ToString();
            value = (int)miscItemData[i]["value"];


            MiscItemDatabase.Add(new MiscItem(slug, title, description, value));
        }
    }

    private void ConstructAccessoryDatabase()
    {
        string slug;
        string title;
        string description;
        int value;

        string effectID;
        int effectPower;

        for (int i = 0; i < accessoryData.Count; i++)
        {
            slug = accessoryData[i]["slug"].ToString();
            title = accessoryData[i]["title"].ToString();
            description = accessoryData[i]["description"].ToString();
            value = (int)accessoryData[i]["value"];

            effectID = accessoryData[i]["effect"]["effectID"].ToString();
            effectPower = (int)accessoryData[i]["effect"]["effectPower"];

            AccessoryDatabase.Add(new Accessory(slug, title, description, value, effectID, effectPower));
        }
    }

    private void ConstructConsumableDatabase()
    {
        string slug;
        string title;
        string description;
        int value;

        Effect effect;
        List<Effect> effects;

        string effectID;
        int effectPower;

        bool isStackable = false;

        JsonData data = consumableData;

        for (int i = 0; i < data.Count; i++)
        {
            slug = data[i]["slug"].ToString();
            title = data[i]["title"].ToString();
            description = data[i]["description"].ToString();
            value = (int)data[i]["value"];

            switch (data[i]["isStackable"].ToString())
            {
                case "true":
                    isStackable = true;
                    break;
                case "false":
                    isStackable = false;
                    break;
            }

            effectID = data[i]["effect"]["effectID"].ToString();
            effectPower = (int)data[i]["effect"]["effectPower"];

            effect = new Effect(effectID, effectPower);

            effects = new List<Effect>
            {
                effect
            };

            ConsumableDatabase.Add(new Consumable(slug, title, description, value, isStackable, effects));
        }
    }


    /*
    void ConstructEnemyDatabase()
    {
        string slug;
        string title;

        //TODO
        //string description;

        int health;
        int attack;
        int defense;

        for (int i = 0; i < enemyData.Count; i++)
        {
            slug = enemyData[i]["slug"].ToString();
            title = enemyData[i]["title"].ToString();

            health = (int)enemyData[i]["stats"]["health"];
            attack = (int)enemyData[i]["stats"]["attack"];
            defense = (int)enemyData[i]["stats"]["defense"];

            EnemyDatabase.Add(new Enemy(slug,title,health,attack,defense));
        }
    }

    void ConstructCharacterDatabase()
    {
        string slug;
        string title;

        //TODO
        //string description;

        int health;
        int attack;
        int defense;

        for (int i = 0; i < characterData.Count; i++)
        {
            slug = characterData[i]["slug"].ToString();
            title = characterData[i]["title"].ToString();

            health = (int)characterData[i]["stats"]["health"];
            attack = (int)characterData[i]["stats"]["attack"];
            defense = (int)characterData[i]["stats"]["defense"];

            CharacterDatabase.Add(new Character(title, slug, health, attack, defense));
        }
    }
    */

    public NPC GetNPCBySlug(string slug)
    {
        //Debug.Log("NPC Database size: " + NPCDatabase.Count);
        for (int i = 0; i < NPCDatabase.Count; i++)
        {
            if (NPCDatabase[i].Slug.Equals(slug))
            {
                return new NPC(NPCDatabase[i]);
            }
        }
        Debug.Log("NPC of slug: " + slug + " Not Found.");
        return null;
    }

    public Armor GetArmorBySlug(string slug)
    {
        for (int i = 0; i < ArmorDatabase.Count; i++)
        {
            if (ArmorDatabase[i].Slug.Equals(slug))
            {
                return new Armor(ArmorDatabase[i]);
            }
        }
        Debug.Log("Armor of slug: " + slug + " Not Found.");
        return null;
    }

    public Weapon GetWeaponBySlug(string slug)
    {
        for (int i = 0; i < WeaponDatabase.Count; i++)
        {
            if (WeaponDatabase[i].Slug.Equals(slug))
            {
                return WeaponDatabase[i];
            }
        }
        Debug.Log("Weapon of slug: " + slug + " Not Found.");
        return null;
    }

    public MiscItem GetMiscItemBySlug(string slug)
    {
        for (int i = 0; i < MiscItemDatabase.Count; i++)
        {
            if (MiscItemDatabase[i].Slug.Equals(slug))
            {
                return MiscItemDatabase[i];
            }
        }
        Debug.Log("MiscItem of slug: " + slug + " Not Found.");
        return null;
    }

    public Accessory GetAccessoryBySlug(string slug)
    {
        for (int i = 0; i < AccessoryDatabase.Count; i++)
        {
            if (AccessoryDatabase[i].Slug.Equals(slug))
            {
                return AccessoryDatabase[i];
            }
        }
        Debug.Log("Accessory of slug: " + slug + " Not Found.");
        return null;
    }

    public Consumable GetConsumableBySlug(string slug)
    {
        for (int i = 0; i < ConsumableDatabase.Count; i++)
        {
            if (ConsumableDatabase[i].Slug.Equals(slug))
            {
                Debug.Log("Returned " + ConsumableDatabase[i].Name);
                return new Consumable(ConsumableDatabase[i]);
            }
        }
        Debug.Log("Consumable of slug: " + slug + " Not Found.");
        return null;
    }
    /*
    public Enemy GetEnemyBySlug(string slug)
    {
        for (int i = 0; i < EnemyDatabase.Count; i++)
        {
            if (EnemyDatabase[i].GetSlug().Equals(slug))
            {
                return EnemyDatabase[i];
            }
        }
        Debug.Log("Enemy of slug: " + slug + " Not Found.");
        return null;
    }

    public Character GetCharacterBySlug(string slug)
    {
        for (int i = 0; i < CharacterDatabase.Count; i++)
        {
            if (CharacterDatabase[i].GetSlug().Equals(slug))
            {
                return CharacterDatabase[i];
            }
        }
        Debug.Log("Enemy of slug: " + slug + " Not Found.");
        return null;
    }
    */
}