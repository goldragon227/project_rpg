﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData {

    private InventoryData _inventory;
    private NPC npc;

    public NPC Player
    {
        get
        {
            return npc;
        }

        set
        {
            npc = value;
        }
    }

    public InventoryData Inventory
    {
        get
        {
            return _inventory;
        }

        set
        {
            _inventory = value;
        }
    }
}
