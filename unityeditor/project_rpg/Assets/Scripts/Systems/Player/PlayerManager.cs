﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

public class PlayerManager : MonoBehaviour {

    static PlayerManager instance = null;

    //[Header("Player Character Stats")]
    //public int maxHealth;
    //public int maxMagicPower;
    //public int maxLust;

    //private int health;
    //private int magicPower;
    //private int lust;

    //public int attack;
    //public int defense;

    public float sprintSpeed;
    public float walkSpeed;

    private float playerSpeed;

    //public string characterName;
    //public string slug;

    private NPC _npc;

    [Header("Settings")]
    
    public float _raycastRadius = 20f;

    [Header("Managers / Controllers")]

    private GameObject GM;

    //public BattleController battleScript;
    private BattleManager _battleManager;
    private JSONManager _database;
    private DebugUI _debugUI;

    private Animator _playerAnimator;
    private Rigidbody2D _playerRigidbody2D;

    private PlayerData _data;


    // Use this for initialization
    void Start () {

        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            print("Duplicate player self-destructing!");
        }
        else
        {
            GM = GameObject.Find("_GM").gameObject;
            _database = GM.GetComponent<JSONManager>();
            _battleManager = GM.GetComponent<BattleManager>();
            _debugUI = GM.GetComponent<DebugUI>();


            instance = this;
            DontDestroyOnLoad(gameObject);

            _playerRigidbody2D = gameObject.GetComponent<Rigidbody2D>();
            _playerAnimator = gameObject.GetComponent<Animator>();

            npc = _database.GetNPCBySlug("player");

            _data = new PlayerData
            {
                Inventory = new InventoryData(),
                Player = npc
            };


            //print("Character 1: " + party[0].GetCharacterName() + "| Character 2: " + party[1].GetCharacterName() + "| Character 3: " + party[2].GetCharacterName() + "| Character 4: " + party[3].GetCharacterName());

        }
    }


	
	// Update is called once per frame
	void Update () {

	}


    //Movement

    public void SetSprinting()
    {
        playerSpeed = sprintSpeed;
    }

    public void SetWalking()
    {
        playerSpeed = walkSpeed;
    }

    public void MoveNorthWest()
    {
        _playerRigidbody2D.transform.position += Vector3.left * playerSpeed * Time.deltaTime;
        _playerRigidbody2D.transform.position += Vector3.up * playerSpeed * Time.deltaTime;
        SetPlayerAnimatorState(8);
    }

    public void MoveSouthWest()
    {
        _playerRigidbody2D.transform.position += Vector3.left * playerSpeed * Time.deltaTime;
        _playerRigidbody2D.transform.position += Vector3.down * playerSpeed / 2 * Time.deltaTime;
        SetPlayerAnimatorState(6);
    }

    public void MoveNorthEast()
    {
        _playerRigidbody2D.transform.position += Vector3.up * playerSpeed * Time.deltaTime;
        _playerRigidbody2D.transform.position += Vector3.right * playerSpeed * Time.deltaTime;
        SetPlayerAnimatorState(2);
    }

    public void MoveSouthEast()
    {
        _playerRigidbody2D.transform.position += Vector3.down * playerSpeed * Time.deltaTime;
        _playerRigidbody2D.transform.position += Vector3.right * playerSpeed * Time.deltaTime;
        SetPlayerAnimatorState(4);
    }

    public void MoveWest()
    {
        _playerRigidbody2D.transform.position += Vector3.left * playerSpeed * Time.deltaTime;
        SetPlayerAnimatorState(7);
    }

    public void MoveEast()
    {
        _playerRigidbody2D.transform.position += Vector3.right * playerSpeed * Time.deltaTime;
        SetPlayerAnimatorState(3);
    }

    public void MoveNorth()
    {
        _playerRigidbody2D.transform.position += Vector3.up * playerSpeed * Time.deltaTime;
        SetPlayerAnimatorState(1);
    }

    public void MoveSouth()
    {
        _playerRigidbody2D.transform.position += Vector3.down * playerSpeed * Time.deltaTime;
        SetPlayerAnimatorState(5);
    }

    public void Idle()
    {
        SetPlayerAnimatorState(0);
    }



    public void RaycastForEnemies()
    {
        Vector2 position = new Vector2(_playerRigidbody2D.transform.position.x, _playerRigidbody2D.transform.position.y);
        RaycastHit2D[] hits = Physics2D.CircleCastAll(position, 10.0f, Vector2.zero);
        _debugUI.DisplayFoundEnemies(hits);
    }

    //private void OnLevelWasLoaded(int level)
    //{

    //    Debug.Log("From" + lastLevel);

    //    GameObject enterPoint = GameObject.Find("From" + lastLevel);
    //    Debug.Log(enterPoint.name + " X: " + enterPoint.transform.position.x + " Y: " + enterPoint.transform.position.y);
    //    playerRigidbody2D = GetComponent<Rigidbody2D>();

    //    if (enterPoint != null)
    //    {
    //        Debug.Log("Rigidbody Check: " + playerRigidbody2D.name);
    //        _playerRigidbody2D.transform.position = enterPoint.transform.position;
    //    }

    //}


    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Enemy":
                //Debug.Log("Collide");
                StartBattle();
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*
        switch (collision.tag) {
            case "Enemy":
                Debug.Log("Collide");
                battleScript.BattleStart(collision.gameObject);
                break;
            default:
                break;
        }
        */
    }


    void StartBattle()
    {
        Debug.Log("Battle Called");

        Vector2 position = new Vector2(_playerRigidbody2D.transform.position.x, _playerRigidbody2D.transform.position.y);
        RaycastHit2D[] hits = Physics2D.CircleCastAll(position, _raycastRadius, Vector2.zero);
        _debugUI.DisplayFoundEnemies(hits);

        List<GameObject> allies = new List<GameObject>();
        List<GameObject> enemies = new List<GameObject>();

        foreach (RaycastHit2D element in hits)
        {
            GameObject tempObject = element.collider.gameObject;


            switch (tempObject.tag)
            {
                case "Player":
                    allies.Add(tempObject);
                    break;
                case "Ally":
                    allies.Add(tempObject);
                    break;
                case "Enemy":
                    if (tempObject.GetComponent<Enemy>().IsFollowingPlayer)
                    {
                        enemies.Add(tempObject);
                    }
                    break;
            }
        }

        _debugUI.DisplayAllyInfo(allies);
        _debugUI.DisplayEnemyInfo(enemies);

        _battleManager.PlayerRequestBattle(allies, enemies);
    }

    void SetPlayerAnimatorState(int state)
    {
        _playerAnimator.SetInteger("state", state);
    }

    public NPC npc
    {
        get
        {
            return _npc;
        }

        set
        {
            _npc = value;
        }
    }

    public PlayerData Data
    {
        get
        {
            return _data;
        }
    }
}