﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle{

    List<GameObject> _allyGameObjects;
    List<GameObject> _enemyGameObjects;

    List<NPC> _allies;
    List<NPC> _enemies;

    private bool _isPlayerPartyDead;
    private bool _isEnemyPartyDead;

    private GameObject _enemySelected;
    private GameObject _allySelected;

    private NPC _allyAttackingNPC;
    private NPC _enenmyDefendingNPC;

    private int _maxTotalSpeed = 10;

    int _turn = 0;

    public Battle(List<GameObject> allies, List<GameObject> enemies)
    {
        AllyGameObjects = allies;
        EnemyGameObjects = enemies;

        _allies = new List<NPC>();
        _enemies = new List<NPC>();

        foreach (GameObject element in AllyGameObjects)
        {
            NPC elementNPC;
            switch (element.tag)
            {
                case "Player":
                    //NPC npc = element.GetComponent<PlayerController>().NPC;
                    //Debug.Log("NPC NAME: " + npc.Name);
                    elementNPC = element.GetComponent<PlayerManager>().npc;
                    Allies.Add(elementNPC);
                    elementNPC.GameObject = element;
                    break;
                case "Ally":
                    elementNPC = element.GetComponent<Follower>().NPC;
                    Allies.Add(elementNPC);
                    elementNPC.GameObject = element;
                    break;
            }
            
            
        }

        foreach(GameObject element in EnemyGameObjects)
        {
            Enemies.Add(element.GetComponent<Enemy>().NPC);
        }
    }

    public List<NPC> Allies
    {
        get
        {
            return _allies;
        }

        set
        {
            _allies = value;
        }
    }

    public List<NPC> Enemies
    {
        get
        {
            return _enemies;
        }

        set
        {
            _enemies = value;
        }
    }

    public List<GameObject> AllyGameObjects
    {
        get
        {
            return _allyGameObjects;
        }

        set
        {
            _allyGameObjects = value;
        }
    }

    public List<GameObject> EnemyGameObjects
    {
        get
        {
            return _enemyGameObjects;
        }

        set
        {
            _enemyGameObjects = value;
        }
    }

    public GameObject EnemySelected
    {
        get
        {
            return _enemySelected;
        }

        set
        {
            _enemySelected = value;
        }
    }

    public GameObject AllySelected
    {
        get
        {
            return _allySelected;
        }

        set
        {
            _allySelected = value;
        }
    }

    public int MaxTotalSpeed
    {
        get
        {
            return _maxTotalSpeed;
        }

        set
        {
            _maxTotalSpeed = value;
        }
    }
}
