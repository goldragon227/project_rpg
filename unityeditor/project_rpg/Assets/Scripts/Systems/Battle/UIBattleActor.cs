﻿
using System;
using UnityEngine;
using UnityEngine.UI;

public class UIBattleActor {

    private GameObject UIObject;

    private Text _name;
    private Text _health;
    private Text _magic;
    private Image _thumbnail;

    private GameObject _selectorArrow;

    public UIBattleActor(Text name, Text health, Text magic, Image thumbnail, GameObject select)
    {
        this._name = name;
        this._health = health;
        this._magic = magic;
        this._thumbnail = thumbnail;
        this._selectorArrow = select;
    }

    public UIBattleActor(Text name, Text health, Text magic, Image thumbnail)
    {
        this._name = name;
        this._health = health;
        this._magic = magic;
        this._thumbnail = thumbnail;
    }

    public void setNameText(string value)
    {
        this._name.text = value;
    }

    public void setHealthText(string value)
    {
        this._health.text = value;
    }

    public void setMagicText(string value)
    {
        this._magic.text = value;
    }

    public void setThumbnailImageSprite(Sprite sprite)
    {
        this._thumbnail.sprite = sprite;
    }

    /*
    public void SetSelector(bool state)
    {
        this._selectorArrow.SetActive(state);
    }
    */

    public GameObject GetSelector()
    {
        return this._selectorArrow;
    }
}
