﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class for BattleAction
/* Player GameObject 
 * Enemy GameObject
 * Type of attack
 * Skill ID
 * Item ID
 * Skill for healing/buffing?
 * Index of Player and Enemy?
 */

public class BattleAction
{
    private GameObject _attacker;
    private GameObject _defender;
    private BattleManager.ActionType _action;

    private int _attackerIndex;
    private int _defenderIndex;

    public BattleAction(GameObject attacker, GameObject defender, BattleManager.ActionType action)
    {
        Attacker = attacker;
        Defender = defender;
        Action = action;
    }


    public GameObject Attacker
    {
        get
        {
            return _attacker;
        }

        set
        {
            _attacker = value;
        }
    }

    public GameObject Defender
    {
        get
        {
            return _defender;
        }

        set
        {
            _defender = value;
        }
    }

    public BattleManager.ActionType Action
    {
        get
        {
            return _action;
        }

        set
        {
            _action = value;
        }
    }
}
