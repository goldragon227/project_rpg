﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUI : MonoBehaviour {

    private BattleManager battleManager;

    private int _enemySelectionIndex = 0;

    public GameObject battleUIGameObject;

    public GameObject targetPrefab;
    private GameObject _target;

    public Camera _currentCamera;

	// Use this for initialization
	void Start () {
        battleManager = gameObject.GetComponent<BattleManager>();

	}

    void Update()
    {

    }

    /*
    void StateMachine()
    {

        switch (battleManager.BattleState)
        {
            case BattleManager.BattleStates.Idle:
                break;
            case BattleManager.BattleStates.Initialize:
                Initialize();
                break;
            case BattleManager.BattleStates.PlayerStart:
                PlayerStart();
                break;
            case BattleManager.BattleStates.PlayerAttack:
                PlayerAttack();
                //Debug.Log("PLAYER ATTACK");
                break;
            case BattleManager.BattleStates.PlayerSkill:
                break;
            case BattleManager.BattleStates.PlayerItem:
                break;
            case BattleManager.BattleStates.PlayerRun:
                break;
            case BattleManager.BattleStates.EnemyTurn:
                break;
            case BattleManager.BattleStates.ResolveTurn:
                break;
            case BattleManager.BattleStates.NextTurn:
                break;
            case BattleManager.BattleStates.PlayerWin:
                break;
            case BattleManager.BattleStates.PlayerLose:
                break;
        }
    }
    */

    /* instentiate an place the target over the 0th enemy
    * Make it inactive
    */
    public void Initialize()
    {
        Debug.Log("INITIALIZE TARGET");
        battleManager.Battle.AllyGameObjects = battleManager.Battle.AllyGameObjects;
        battleManager.Battle.EnemyGameObjects = battleManager.Battle.EnemyGameObjects;

        _enemySelectionIndex = 0;

        //Gives a refrence of an enemy for the Battle class
        battleManager.Battle.EnemySelected = battleManager.Battle.EnemyGameObjects[_enemySelectionIndex];

        //Finds the selected enemy and gets its position reletive to the screen
        Vector3 screenPosition = _currentCamera.WorldToScreenPoint(battleManager.Battle.EnemySelected.GetComponent<Transform>().position);


        if (_target == null)
        {
            //Creates a target on the canvas centered over the first selected enemy
            _target = Instantiate(targetPrefab, screenPosition, Quaternion.identity, battleUIGameObject.transform);
            _target.name = "Target";
        }

        _target.GetComponent<Transform>().position = screenPosition;
    }

    /* Ability to select enemy
     * 
     */

    public void PlayerStart()
    {
        EnemySelector();

        if (Input.GetKeyDown(KeyCode.W))
        {
            battleManager.BattleState = BattleManager.BattleStates.PlayerAttack;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            //battleController.BattleState = BattleControllerOverworld.BattleStates.PlayerSkill;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            //battleController.BattleState = BattleControllerOverworld.BattleStates.PlayerItem;
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            //battleController.BattleState = BattleControllerOverworld.BattleStates.PlayerEscape;
        }
    }

    public void PlayerAttack()
    {
        EnemySelector();

        if (Input.GetKeyDown(KeyCode.Return))
        {
            //Will set the value of something to refrence the GameObject of the Enemy that the player is attacking
            battleManager.Battle.EnemySelected = battleManager.Battle.EnemyGameObjects[_enemySelectionIndex];
            //battleManager.PlayerChooseAttack();
            battleManager.PlayerAttack();
        }
    }

    private void EnemySelector()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //No reason to call this if there is only one enemy to target
            if (battleManager.Battle.EnemyGameObjects.Count > 1)
            {
                //Debug.Log("Right arrow called");
                //Debug.Log("Enemy count: " + _enemies.Count + " enemySelectionIndex: " + _enemySelectionIndex);
                _enemySelectionIndex++;

                //If the enemy selected is greater than the number of enemies then switch back to the first enemy
                if (_enemySelectionIndex >= battleManager.Battle.EnemyGameObjects.Count)
                {
                    _enemySelectionIndex = 0;
                }

                battleManager.Battle.EnemySelected = battleManager.Battle.EnemyGameObjects[_enemySelectionIndex];

                //Debug.Log(_enemies[_enemySelectionIndex].GetComponent<Transform>().position);
                //Debug.Log(_currentCamera.backgroundColor);

                Vector3 worldPosition = battleManager.Battle.EnemySelected.GetComponent<Transform>().position;
                Vector3 screenPosition = _currentCamera.WorldToScreenPoint(worldPosition);

                

                _target.GetComponent<Transform>().position = screenPosition;
            }

        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (battleManager.Battle.EnemyGameObjects.Count > 1)
            {
                //Debug.Log("Left arrow called");
                //Debug.Log("Enemy count: " + _enemies.Count + " enemySelectionIndex: " + _enemySelectionIndex);
                _enemySelectionIndex--;

                if (_enemySelectionIndex < 0)
                {
                    _enemySelectionIndex = battleManager.Battle.EnemyGameObjects.Count - 1;
                }

                //Debug.Log(_enemies[_enemySelectionIndex].GetComponent<Transform>().position);

                battleManager.Battle.EnemySelected = battleManager.Battle.EnemyGameObjects[_enemySelectionIndex];

                Vector3 worldPosition = battleManager.Battle.EnemySelected.GetComponent<Transform>().position;
                Vector3 screenPosition = _currentCamera.WorldToScreenPoint(worldPosition);

                _target.GetComponent<Transform>().position = screenPosition;
            }
        }
    }
}
