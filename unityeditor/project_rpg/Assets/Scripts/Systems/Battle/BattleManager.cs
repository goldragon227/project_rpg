﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour {

    GameManager gameManager;

    BattleUI battleUI;
    DebugUI debugUI;

    JSONManager database;

    /* -Battle States-
     * 
     * Idle:            No battle going on.
     * Initialize:      Load all ally data and enemy data.
     * PlayerStart:     Choose the options.
     * PlayerAttack     Targeting menu, Default attack.
     * PlayerSkill:     Select skill menu.
     * PlayerItem:      Select item menu.
     * PlayerRun:       Used to let the player try and run away.
     * EnemyTurn:       Use for enemy AI to decide which player to attack.
     * ResolveTurn:     Use to do the calculations, animations, etc for the battle.
     * NextTurn:        Use for reseting varibles and etc.
     * PlayerWin:       Add loot to the player's inventory-
     *                  experience distribution and etc here.
     * PlayerLose:      Handle player loss here
     * 
     */

    public enum ActionType
    {
        BasicAttack,  //Magic/Ranged/Magic depends on weapon type
        Skill,  //Need skill ID
        Item    //Item ID or something

    }

    public enum BattleStates
    {
        Idle,
        Initialize,
        CalcTurn,
        PlayerTurn,
        PlayerAttack,
        PlayerSkill,
        PlayerItem,
        PlayerRun,
        EnemyTurn,
        ResolveTurn,
        NextTurn,
        PlayerWin,
        PlayerLose,
        PlayerEscape
    }

    BattleStates _battleState = BattleStates.Idle;
    private Battle _battle;

    private int _currentAttackingAlly = 0;

    private NPC _nextAttacker;


    //Make sure to remove the attacker from this list after they attack
    private List<NPC> _attackersList = new List<NPC>();
    private Queue<NPC> _attackersQueue;

    /* Controllers
     * 
     * BattleUI
     * BattleControls
     * 
     */
    

    // Use this for initialization
    void Start () {
        debugUI = gameObject.GetComponent<DebugUI>();
        battleUI = gameObject.GetComponent<BattleUI>();
        database = gameObject.GetComponent<JSONManager>();
        gameManager = gameObject.GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
        switch (GameManager.GameState)
        {
            case GameManager.GameStates.BATTLE:
                StateMachine();
                break;
            case GameManager.GameStates.PAUSED:
                break;
        }
    }

    //Branching based on the state of the battle
    private void StateMachine()
    {
        switch (BattleState)
        {
            case BattleStates.Idle:
                break;
            case BattleStates.Initialize:
                Debug.Log("Init battle");
                //Initialize();
                BattleState = BattleStates.CalcTurn;
                battleUI.Initialize();
                break;
            case BattleStates.PlayerTurn:
                battleUI.PlayerStart();
                //Log("Player turn");
                break;
            case BattleStates.PlayerAttack:
                battleUI.PlayerAttack();
                break;
            case BattleStates.PlayerSkill:
                break;
            case BattleStates.PlayerItem:
                break;
            case BattleStates.PlayerRun:
                break;
            case BattleStates.EnemyTurn:
                EnemyTurn();
                break;
            case BattleStates.ResolveTurn:
                break;
            case BattleStates.NextTurn:
                break;
            case BattleStates.PlayerWin:
                PlayerWin();
                break;
            case BattleStates.PlayerLose:
                break;
            case BattleStates.PlayerEscape:
                break;
            case BattleStates.CalcTurn:
                CalcTurn();
                break;
        }
    }

    /* Player sends a Raycast array to the Battle controller and asks to start a battle
     */

    public void PlayerRequestBattle(List<GameObject> allies, List<GameObject> enemies)
    {
        Battle = new Battle(allies, enemies);

        Debug.Log("Battle requested");
        BattleState = BattleStates.Initialize;
        GameManager.GameState = GameManager.GameStates.BATTLE;
    }

    /* Does all the initialization needed for the battle
     * Update the UI for the battle
     * Load in all the enemies and players
     * Tell the GameManager that the battle started
     */

    void Initialize()
    {
        //Debug.Log("I BETTER BE CALLED TOO");
        //Debug.Log(BattleState);
    }

    public BattleStates BattleState
    {
        get
        {
            return _battleState;
        }

        set
        {
            _battleState = value;
            Debug.Log("BattleController state is now: " + _battleState);
        }
    }

    public Battle Battle
    {
        get
        {
            return _battle;
        }

        set
        {
            _battle = value;
        }
    }

    private void CalcTurn()
    {
        //Debug.Log(BattleState);
        /* Needs all enemies and player characters gameobjects/NPCs
         * Adds to some value the NPC's speed
         * When that speed meets or exceeds some value that NPC can attack
         * If another NPC also meets or exceeds that value at the same time 
         * the NPC with the higher speed wins and goes first
         * (Some ability in the future that allows the slower NPC to win the tie?)
         *
         * Pause the timer
         */

        //_battle.Allies;
        //_battle.Enemies;

        /* Check here if any NPC meets or exceeds the required speedTotal to 
         * attack, if there is more then one that meets/exceeds the limit make a list of them all
         * and the one with the highest speedTotal goes first. Subtract the battleSpeedTotal from
         * the NPC's speedTotal.
         * 
         * THIS ALL ASSUMES THERE ARE NO ATTACKERS IN THE QUEUE OR ATTACKERS
         */


        foreach (NPC element in _battle.Allies)
        {
            element.SpeedTotal += element.Speed * Time.deltaTime;
            //Debug.Log(element.Name + "  has total: " + element.SpeedTotal + " added: " + element.Speed);
            if (element.SpeedTotal > _battle.MaxTotalSpeed)
            {
                //Debug.Log(element.Name + " added as an attacker.");
                _attackersList.Add(element);
            }
        }

        foreach (NPC element in _battle.Enemies)
        {
            element.SpeedTotal += element.Speed * Time.deltaTime;
            //Debug.Log(element.Name + "  has total: " + element.SpeedTotal + " added: " + element.Speed);

            if (element.SpeedTotal > _battle.MaxTotalSpeed)
            {
                //Debug.Log(element.Name + " added as an attacker.");
                _attackersList.Add(element);
            }
        }

        if (_attackersList.Count > 0)
        {
            _attackersQueue = new Queue<NPC>();
            /* Check if the enemy is attacking or the allies are attacking
             * Which also requires that the order of the NPCs is correct
             * The NPC with the largest totalSpeed goes first
             * Compare a bassically infinte amount of numbers and sort them based on who attacks first
             */

            if(_attackersList.Count > 1)
            {
                //Loop through all the attackers and find the one with the most
                //built up speed and add them to the _attackersQueue
                for (int i = 0; i < _attackersList.Count; i++)
                {
                    SortNPCIndex(i, _attackersQueue);
                }
            }

            //No need to sort based on speed if there is only one attacker
            else if (_attackersList.Count == 1)
            {
                _attackersQueue.Enqueue(_attackersList[0]);
            }

            SwitchAttacker();
            //At this point there should be a Queue of one or more NPCs that can attack
            //And the state is set to the first attackers turn

            //reset the attacker list 
            _attackersList = new List<NPC>();
        }
    }

    private void SortNPCIndex(int startIndex, Queue<NPC> attackersQueue)
    {
        NPC largestSpeed;
        NPC npc;
        largestSpeed = _attackersList[startIndex];

        for (int i = startIndex; i < _attackersList.Count; i++)
        {
            npc = _attackersList[i];

            if (largestSpeed.SpeedTotal < npc.SpeedTotal)
            {
                largestSpeed = npc;
            }
        }

        attackersQueue.Enqueue(largestSpeed);
    }

    private void PlayerTurn()
    {

    }


    //TODO Implement death of enemies
    public void PlayerAttack()
    {
        NPC defender = _battle.EnemySelected.GetComponent<Enemy>().NPC;

        DamageNPC(_nextAttacker, defender, ActionType.BasicAttack);
        //Check if there is any more attackers

        _nextAttacker.SpeedTotal = _nextAttacker.SpeedTotal - Battle.MaxTotalSpeed;

        if (defender.IsDead)
        {
            Debug.Log(defender.Name + " died.");
            Battle.Enemies.Remove(defender);
            Battle.EnemyGameObjects.Remove(defender.GameObject);
            Debug.Log(Battle.Enemies.Count + " enemies remaining.");

            //foreach(NPC element in Battle.Enemies)
            //{

            //}

            //foreach (GameObject element in Battle.EnemyGameObjects)
            //{

            //}
        }

        //Remove the dead enemies from the Enemies array

        if (_attackersQueue.Count > 0)
        {
            SwitchAttacker();
        }
        else
        {
            BattleState = BattleStates.CalcTurn;
        }

        


        debugUI.DisplayAllyInfo(Battle.AllyGameObjects);
        debugUI.DisplayEnemyInfo(Battle.EnemyGameObjects);

        if (Battle.Enemies.Count == 0)
        {
            BattleState = BattleStates.PlayerWin;
        }

    }

    //Implement death of characters
    private void EnemyTurn()
    {
        foreach(NPC element in Battle.Allies)
        {
            if(element.Health > 0)
            {
                DamageNPC(_nextAttacker, element, ActionType.BasicAttack);
                break;
            }
        }

        _nextAttacker.SpeedTotal -= Battle.MaxTotalSpeed;

        if (_attackersQueue.Count > 0)
        {
            SwitchAttacker();
        } else
        {
            BattleState = BattleStates.CalcTurn;
        }

        debugUI.DisplayAllyInfo(_battle.AllyGameObjects);
        debugUI.DisplayEnemyInfo(_battle.EnemyGameObjects);
    }

    //Takes in the attacker and defender and applies damage
    private void DamageNPC(NPC attacker, NPC defender, ActionType actionType)
    {
        int attackerDamage = attacker.Attack - defender.Defense;

        if (attackerDamage < 0)
        {
            attackerDamage = 0;
        }

        Debug.Log(attacker.Name + " does " + attackerDamage + " damage to " + defender.Name);

        defender.Health = defender.Health - attackerDamage;

        if(defender.Health <= 0)
        {
            defender.IsDead = true;
        }

    }

    private void SwitchAttacker()
    {
        _nextAttacker = _attackersQueue.Dequeue();
        Debug.Log("Next attacker is " + _nextAttacker.Name);

        switch (_nextAttacker.Faction)
        {
            case Faction.Player:
                BattleState = BattleStates.PlayerTurn;
                break;
            case Faction.Enemy:
                BattleState = BattleStates.EnemyTurn;
                break;
        }
    }

    private void PlayerWin()
    {
        foreach(GameObject element in _battle.EnemyGameObjects)
        {
            Destroy(element);
        }

        BattleState = BattleStates.Idle;
        GameManager.GameState = GameManager.GameStates.OVERWORLD;
    }

}
