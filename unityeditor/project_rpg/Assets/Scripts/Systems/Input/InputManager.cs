﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public PlayerManager player;
    private GameManager game;
    //private BattleController battle; DEPRECATED

    private int _playerBattleControlIndex = 0;
    private int _playerBattleEnemyMarkerIndex = 0;

    // Use this for initialization
    void Start () {
        game = gameObject.GetComponent<GameManager>();
        //battle = gameObject.GetComponent<BattleController>(); DEPRECATED
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        pauseToggle();

        switch (GameManager.GameState)
        {
            case GameManager.GameStates.OVERWORLD:
                PlayerOverworld();
                break;
        }
    }

    //Player Overworld Controls

    private void PlayerOverworld()
    {

        if (Input.GetKey(KeyCode.LeftShift))
        {
            player.SetSprinting();
        }
        else
        {
            player.SetWalking();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            player.RaycastForEnemies();
        }


        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W)) //Northwest Int 8
        {
            player.MoveNorthWest();
        }
        else if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S)) //Southwest 6
        {
            player.MoveSouthWest();
        }
        else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D)) //Northeast 2
        {
            player.MoveNorthEast();
        }
        else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S)) //Southeast 4
        {
            player.MoveSouthEast();
        }
        else if (Input.GetKey(KeyCode.A)) //West 7
        {
            player.MoveWest();
        }
        else if (Input.GetKey(KeyCode.D)) //East 3
        {
            player.MoveEast();
        }
        else if (Input.GetKey(KeyCode.W)) //North 1
        {
            player.MoveNorth();
        }
        else if (Input.GetKey(KeyCode.S)) //South 5
        {
            player.MoveSouth();
        }
        else
        {
            player.Idle();
        }
    }

    private void pauseToggle()
    {
        if (Input.GetKeyDown(KeyCode.Slash))
        {
            GameManager.TogglePause();
        }
    }

    //Battle Controls

    /*
    public void MoveMarkerToNextLivingEnemy()
    {
        if (battle.GetEnemies()[_playerBattleEnemyMarkerIndex].IsDead)
        {
            _playerBattleEnemyMarkerIndex = battle.NextLivingEnemy();
        }
    }

    public void BattlePlayer()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (_playerBattleControlIndex != 3)
            {
                UI.SetBattleControls(_playerBattleControlIndex, false);
                _playerBattleControlIndex++;
                UI.SetBattleControls(_playerBattleControlIndex, true);
            }

        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            if (_playerBattleControlIndex != 0)
            {
                UI.SetBattleControls(_playerBattleControlIndex, false);
                _playerBattleControlIndex--;
                UI.SetBattleControls(_playerBattleControlIndex, true);
            }

        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            switch (_playerBattleControlIndex)
            {
                case 0:
                    battle.BattleState = BattleController.BattleStates.PlayerAttack;
                    UI.SetBattleControlsVisisbility(false);
                    break;
                case 1:
                    //battle.SetBattleState(BattleController.BattleStates.PlayerSkill);
                    break;
                case 2:
                    break;
                case 3:
                    break;
            }

        }
    }

    public void BattlePlayerAttack()
    {
        if (UI.GetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex) == false)
        {
            UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, true);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            if ((_playerBattleEnemyMarkerIndex != 3) && (!battle.GetEnemies()[_playerBattleEnemyMarkerIndex + 1].IsDead))
            {
                UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, false);
                _playerBattleEnemyMarkerIndex++;
                UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, true);
            }
            else if (!battle.GetEnemies()[_playerBattleEnemyMarkerIndex].IsDead)
            {
                int nextLivingEnemy = battle.NextLivingEnemy(_playerBattleEnemyMarkerIndex, true);
                UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, false);
                _playerBattleEnemyMarkerIndex = nextLivingEnemy;
                UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, true);
            }
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            if (_playerBattleEnemyMarkerIndex != 0 && (!battle.GetEnemies()[_playerBattleEnemyMarkerIndex - 1].IsDead))
            {
                UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, false);
                _playerBattleEnemyMarkerIndex--;
                UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, true);
            }
            else if (!battle.GetEnemies()[_playerBattleEnemyMarkerIndex].IsDead)
            {
                int nextLivingEnemy = battle.NextLivingEnemy(_playerBattleEnemyMarkerIndex, false);
                UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, false);
                _playerBattleEnemyMarkerIndex = nextLivingEnemy;
                UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, true);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            battle.CharacterAttack(_playerBattleEnemyMarkerIndex, BattleAttack.AttackTypes.Melee);
            UI.SetBattleEnemyMarkerVisibility(_playerBattleEnemyMarkerIndex, false);
        }
    }
    */
}
